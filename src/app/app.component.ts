import { Component } from '@angular/core';
import { NewsRes } from './service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!'
  constructor(public service: NewsRes) {
    service.get({id: 14}, (receivedNews: any) => {
      console.log(receivedNews);
      // do some magic after receiving news
    });  }
}
